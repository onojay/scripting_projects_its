#!/usr/bin/env python3
# Project 6
# Python 3.7.1
# A script that attempts to retrieve a url that is supplied by the user.
# It prints out the status code after the retrieval attempt and also prints the full url as additional information.

import requests, sys

print('Kindly enter url at the prompt below')

while True:
    url = input('>>>')

    if url.lower() in ['x','q']:
        sys.exit()

    try: 
        res = requests.get(url, allow_redirects=False)
        print("The status code is " + str(res.status_code))
        print("The requested url is " + str(res.url))
    except requests.exceptions.MissingSchema:
        print("Kindly enter the full url and include the protocol eg http://")
    except requests.exceptions.ConnectionError:
        print("The url you entered cannot be reached")
        print("Kindly enter another valid url")
    except Exception as e:
        print (" Your url generated an error of this kind: " + e.__class__.__name__)
        print ("Kindly enter a valid url")