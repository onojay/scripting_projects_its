# John Mensah Onumah
# Python 3.7
# A simple script that takes a file path or name as input from the command line and then reads the entire file and print out files ending with 
# the extension exe.

import sys

# read
file_path = sys.argv[1]

# opening file and storing it in the variable file_opened.
file_opened = open(file_path)

#looping through the file to get the filnames listed within it
for line in file_opened:
    store = line.split(".")
    file_extension = store[-1].strip()
    print(file_extension)

    # check if the file extension is exe
    if file_extension.lower() == "exe":
        print (line)
    
file_opened.close()