import sys
import math


def evaluate_if_log(num):
    cache = num.split('(')
    if cache[0] == "ln":
        return math.log(float(cache[-1].strip(")")))
        sys.exit()
    elif cache[0] == "log":
        return math.log(float(cache[-1].strip(")")),10)
        sys.exit()
    elif "log" in cache[0]:
        return math.log(float(cache[-1].strip(")")),float(cache[0].strip("log")))
        sys.exit()
    else:
        return float(num)
        
    
    #     print (math.log(float(num)))
    #     sys.exit()

while True:
    get_out = input(">>")

    if get_out.upper() in ["X","Q","QUIT","EXIT"]:
        sys.exit()
    
    try:
        if "+" in get_out:
            equation = get_out.split("+")
            print (float(evaluate_if_log(equation[0])) + float(evaluate_if_log(equation[1])))
        elif "-" in get_out:
            equation = get_out.split("-")
            print (float(evaluate_if_log(equation[0])) - float(evaluate_if_log(equation[1])))
        elif "/" in get_out:
            equation = get_out.split("/")
            print (float(evaluate_if_log(equation[0])) / float(evaluate_if_log(equation[1])))
        elif "*" in get_out:
            equation = get_out.split("*")
            print (float(evaluate_if_log(equation[0])) * float(evaluate_if_log(equation[1])))
        else:
            ans = evaluate_if_log(get_out)
            print(ans)
    except ZeroDivisionError:
        print( "you cannot divide by zero. Kindly re-enter non zero divisor")
    except ValueError:
        print ("Please check your inputs and enter valid intergers")
    except Exception:
        print ("Undisclosed error. Please check your values")
