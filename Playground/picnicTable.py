def printPicnic(itemsDIct,leftWidth,rigthWidth):
    print("PICNIC ITEMS".center(leftWidth + rigthWidth,'-'))

    for k, v in itemsDIct.items():
        print (k.ljust(leftWidth,'-') + str(v).rjust(rigthWidth) )

picnicItems = {'sandwhiches': 4, 'apples': 12, 'cups': 4, 'cookies': 8000}

printPicnic(picnicItems,12,5)
printPicnic(picnicItems,20,6)