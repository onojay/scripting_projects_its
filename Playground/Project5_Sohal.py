import math
def functions():
    choice = input('''
1. Addition
2. Subtraction
3. Multiplication
4. Division
5. Natural log
6. Log base 10
7. Log base 2
8. Compound expression
Please enter the type of operation you want to perform:

''')
    try:


        if choice in ["5",'6','7']:
            num1 = float(input('Please enter the first number: '))
        else:
            num1 = float(input('Please enter the first number: '))
            num2 = float(input('Please enter the second number: '))
        
        if choice == '1':
            print(num1,"+",num2,"=", num1+num2)
    
        elif choice == '2':
            print(num1,"-",num2,"=", num1-num2)

        elif choice == '3':
            print(num1,"*",num2,"=", num1*num2)

        elif choice == '4':
            print(num1,"/",num2,"=", num1/num2)
            
        elif choice == '5':
            print(math.log(num1))
            
        elif choice == '6': 
            print(math.log10(num1))
            
        elif choice == '7':
            print(math.log2(num1)) 
            
        elif choice == '8':
            print(num1,"+", math.log(num2),"=", num1+math.log(num2))
                
        else:
            print('You have not typed a valid operator, please run the program again.')

    # Again() function to calculate() function
        re_cal()

    except ZeroDivisionError:
       print("Erorr: You have entered zero in the divisor, operation cannot be completed")
    
    except:
        print("Erorr: Check your inputs again")

# to calculate again after finishing 1 calculation
def re_cal():
    ask = input('''
Do you want to calculate again?
Please type Y for YES or N for NO.
''')

    if ask.upper() == 'Y':
        functions()
    elif ask.upper() == 'N':
        print('See you later.')
    else:
        re_cal()

functions()
