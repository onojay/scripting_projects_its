#! python3
# pw.py - An insecure password locker program

import pyperclip

PASSWORDS = {'email': 'dfdfdfHHUjmsdsh*879343jHuyIiHg',
             'blog': 'djfkdJHJhjjfhJHhi*8909565jhguIUI',
             'luggage': '12345'}

import sys
if len(sys.argv) < 2:
    print ('Usage: python pw.py[account] - copy account password')
    sys.exit()

account = sys.argv[1]

if account in PASSWORDS:
    pyperclip.copy(PASSWORDS[account])
    print ("Password for " + account + " copied to clipboard.")
else:
    print("There is no account name " + account)