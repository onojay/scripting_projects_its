import sys
import math
import operator


def calculate_log(num):
    cache = num.split('(')
    if cache[0] == "ln":
        return math.log(float(cache[-1].strip(")")))
    elif cache[0] == "log":
        return math.log(float(cache[-1].strip(")")),10)
    elif "log" in cache[0]:
        return math.log(float(cache[-1].strip(")")),float(cache[0].strip("log")))
    else:
        return float(num)
        

while True:
    user_input = input(">>")

    if user_input.lower() in ["exit","q","x","quit"]:
        sys.exit()
    
    operand = ""
    data_store = []
    for opera in ["+","-","/","*"]:
        if opera in user_input:
            operand = opera
            data_store = user_input.split(opera)
            print(data_store)
            break
    if len(data_store) == 0:
        data_store.append(user_input)

            
    try:
        if len(data_store) == 1:
            data_store[0] = calculate_log(data_store[0])
        elif "log" in data_store[0]:
            data_store[0] = calculate_log(data_store[0])
            data_store[1] = float(data_store[1])
        elif "log" in data_store[1]:
            data_store[1] = calculate_log(data_store[1])
            data_store[0] = float(data_store[0])
        elif len(data_store) == 2:
            data_store[1] = calculate_log(data_store[1])
        else:
            data_store[0] = float(data_store[0])
            data_store[1] = float(data_store[1])
    except IndexError:
        print ("we have an error")
        

    ops = { "+": operator.add, "-": operator.sub, "/": operator.truediv, "*": operator.mul }
    try:
        print( ops[operand](data_store[0], data_store[1]))
    except ZeroDivisionError:
        print( "you cannot divide by zero. Kindly re-enter non zero divisor")
    except ValueError:
        print ("Please check your inputs and enter valid intergers")
    except KeyError:
        print (data_store[0])
    except TypeError:
        print (operand + str(data_store[1]))
    #except Exception:
        #print ("Undisclosed error. Please check your values")


