# John Mensah Onumah
#  A script the implements a basic calculator in python.
# python3.7

import sys, math

# a function that carries out the basic math operations
def basic_arithmetic(expression):
    if '+' in expression:
        statement = expression.split('+')
        return log_evaluation(statement[0]) + log_evaluation(statement[1])
    elif '-' in expression:
        statement = expression.split('-')
        return log_evaluation(statement[0]) - log_evaluation(statement[1])
    elif '/' in expression:
        statement = expression.split('/')
        return log_evaluation(statement[0]) / log_evaluation(statement[1])
    elif '*' in expression:
        statement = expression.split('*')
        return log_evaluation(statement[0]) * log_evaluation(statement[1])
    else:
        return log_evaluation(expression)
    
#a function that check to see the needs to be a log computation.
def log_evaluation(num):
    if '(' in num:
        log_parts = num.split('(')
        log_parts[1] = int(log_parts[1].strip().strip(')'))
        
        if 'ln' in log_parts[0]:
            return math.log1p(log_parts[1])
        elif 'log2' in log_parts[0]:
            return math.log2(log_parts[1])
        elif 'log' in log_parts[0]:
            return math.log10(log_parts[1])
        else:
            return float(log_parts[1])
    else:
        return float(num)

#the main loop that runs the program
while True:

    user_input = input('''To quit and exit shell kindly type the following: \n"x","q","exit" or "quit" \n>>>''')


    if user_input.lower().strip('') in ['x','q','exit','quit']:
        break

    # a try clause that seeks to prevent the program from crashing and provide useful feedback to the user.
    try:
        print(basic_arithmetic(user_input))
    except ValueError:
        print ("Kindly check your input and provide valid mathematical expression with interger values")
        print ("This program can only handle compound expressions to only one level e.g 3 + log(2) or 2 + 2")
    