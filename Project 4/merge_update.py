import csv

Sample1 = open('Merge-Sample1.csv')
Sample1_Reader = csv.reader(Sample1)
Sample2 = open('Merge-Sample2.csv')
Sample2_Reader = csv.reader(Sample2)

storage=[]

with open('outputfile.csv','w') as fh:
    write_output = csv.writer(fh)
    for row in Sample1_Reader:
        write_output.writerow(row)
        storage.append(row)
    for row in Sample2_Reader:
        if Sample2_Reader.line_num == 1:
            continue
        write_output.writerow(row)
        storage.append(row)



storage.sort(key= lambda row: row[2])

with open('outputfile1.txt','w') as fh:
    column_three_tracker = []
    for row in storage:
        if row[2] not in column_three_tracker and row[2] != '':
            column_three_tracker.append(row[2])
            fh.write(str(row))
            fh.write('\n')