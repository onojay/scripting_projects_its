#!/usr/bin/env python3

# import time module to write timestamps when server goes up or down
import time
from http.server import HTTPServer  # import inbuilt python http server
from server import Server 

HOST_NAME = 'localhost'
PORT_NUMBER = 8000

if __name__ == '__main__':
    httpd = HTTPServer((HOST_NAME,PORT_NUMBER), Server)
    # print time stamp when server is up
    print (time.asctime(),'Server UP - {}:{}'.format(HOST_NAME,PORT_NUMBER))
    print ("\nThis server supports only two commands for security reasons")
    print ("All other commands will not work.")
    print ("\nopen http://127.0.0.1:8000/ls-command to run the linux ls command.")
    print ("open http://127.0.0.1:8000/pwd to run the linux pwd command.")
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print (time.asctime(),'Server DOWN - {}:{}'.format(HOST_NAME,PORT_NUMBER))