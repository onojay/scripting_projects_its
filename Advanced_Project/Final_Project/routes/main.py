import subprocess

def ls_command():
    cmd = ['ls','-al']
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    output, error = proc.communicate()

    output = output.decode('utf-8')

    string = ''
    
    for line in output.split('\n'):
        string = string + line + '\n'
    
    return string


def pwd_command():
    cmd = ['pwd']
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    output, error = proc.communicate()

    output = output.decode('utf-8')
    
    return output

routes = {
    "/ls-command" : ls_command(),
    "/pwd" : pwd_command()
}




