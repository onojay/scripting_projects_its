#! /usr/bin/env python3
#Python3.7

# Creating a simple class that will be used to create employee profile.
# Two subclasses are created, one for workers paid by monthly salary and those paid by the hour.
# Kindly run this script with python 3.7.

#create a class
class Employee_Record():
    #attributes that are set when a new instance is initialized.
    def __init__(self,FirstName,LastName,TelNumber,RoomNumber):
        self.FirstName = FirstName
        self.LastName = LastName
        self.TelNumber = TelNumber
        self.RoomNumber = RoomNumber
    
    #a method that prints out the attributes of the class
    def DirString(self):
        print('{},{} - Room {} - {}'.format(self.FirstName,self.LastName,self.RoomNumber,self.TelNumber))

class Employee_Record_Monthly(Employee_Record):
    def __init__(self,FirstName,LastName,TelNumber,RoomNumber,Salary_Month):
        super().__init__(FirstName,LastName,TelNumber,RoomNumber)
        self.Salary_Month = Salary_Month
    
    def PendingPayment(self):
        return self.Salary_Month

    #a method that prints out the attributes of the class
    def DirString(self):
        print('{},{} - Room {} - {}, Salary: {}'.format(self.FirstName,self.LastName,self.RoomNumber,self.TelNumber,self.Salary_Month))

class Employee_Record_Hourly(Employee_Record):
    def __init__(self,FirstName,LastName,TelNumber,RoomNumber,hr_worked,hr_rate):
        super().__init__(FirstName,LastName,TelNumber,RoomNumber)
        self.hr_worked = hr_worked
        self.hr_rate = hr_rate
    
    def PendingPayment(self):
        Salary =  self.hr_rate * self.hr_worked
        return Salary

    def DirString(self):
        print('{},{} - Room {} - {}, Salary: {:.2f}'.format(self.FirstName,self.LastName,self.RoomNumber,self.TelNumber,(self.hr_rate * self.hr_worked) ))

Salary_workers = {}
Hourly_workers = {}

Company = [Salary_workers, Hourly_workers]



#creating instances of the class
john = Employee_Record_Hourly('John','Onumah','(740)-407-4993','903',40,7.5)
ekow = Employee_Record_Monthly('Ekow','Taylor','(917)-789-0521','1145',4000)
augustine = Employee_Record_Hourly('Augustine','Sekyere','(740)-591-8047','894',80,9.3)

employee_list = [john,ekow,augustine]


for employee in employee_list:
    # This function is known to work only in python 3.7
    if isinstance(employee,Employee_Record_Monthly):
        Salary_workers[employee.LastName] = employee
    else:
        Hourly_workers[employee.LastName] = employee



while True:
    print('\n')
    print('Kindly choose one of the options below: ')
    print("1. Print the company's directory of all employees ")
    print('2. Enter number of hours worked for a single employee. ')
    print('3. Print list of pending payment for all employees. ')
    print('Enter q or x to quit ')

    user_input = input('>>>')

    if user_input.lower() in ['q','x']:
        break
    elif user_input == '1':
        for worker_group in Company:
            for employee in worker_group:
                worker_group[employee].DirString()
    elif user_input == '2':
        print("Here are the employees that are paid by the hour. Kindly type the last name of the employee you want to edit\n")
        for employee in Hourly_workers:
            print(employee)
        employee_name = input(">>>")
        hours_worked = float(input("Kindly enter the number of hours below \n>>>"))
        try: 
            Hourly_workers[employee_name].hr_worked = hours_worked
            print(Hourly_workers[employee_name].hr_worked)
        except KeyError:
            print("The name you entered does not exist in the employee database. Kindly check and re-enter the name\n")
        
    elif user_input == '3':
        for worker_group in Company:
            for employee in worker_group:
                if isinstance(worker_group[employee],Employee_Record_Monthly):
                    print('Pending payment for {} is: {}'.format(employee,worker_group[employee].PendingPayment()))
                elif isinstance(worker_group[employee],Employee_Record_Hourly):
                    print('Pending payment for {} is: {:.2f}'.format(employee,worker_group[employee].PendingPayment()))

