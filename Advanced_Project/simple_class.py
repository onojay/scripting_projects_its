#! /usr/bin/env python3
#Python3.7

# Creating a simple class that will be used to create employee profile.
# A class is created and three instantances initialized.
# The three instances are stored in a list and then printed out using a class method DirString.

#create a class
class Employee_Record():
    #attributes that are set when a new instance is initialized.
    def __init__(self,FirstName,LastName,TelNumber,RoomNumber):
        self.FirstName = FirstName
        self.LastName = LastName
        self.TelNumber = TelNumber
        self.RoomNumber = RoomNumber
    
    #a method that prints out the attributes of the class
    def DirString(self):
        print('{},{} - Room {} - {}'.format(self.FirstName,self.LastName,self.RoomNumber,self.TelNumber))

#creating instances of the class
john = Employee_Record('John','Onumah','(740)-407-4993','903')
ekow = Employee_Record('Ekow','Taylor','(917)-789-0521','1145')
augustine = Employee_Record('Augustine','Sekyere','(740)-591-8047','894')

employee_list = [john,ekow,augustine]


for employee in employee_list:
    employee.DirString()

